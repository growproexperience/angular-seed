var replace = require('replace-in-file');
var buildVersion = getDateStr();
const options = {
    files: 'src/environments/environment.prod.ts',
    from: /{BUILD_VERSION}/g,
    to: buildVersion,
    allowEmptyPaths: false,
};
const servicesOptions = {
    files: 'src/app/services/app.services.ts',
    from: /ServiceMock,\ deps:/g,
    to: 'Service, deps:',
    allowEmptyPaths: false,
};

const indexOptions = {
    files: 'src/index.html',
    from: /<!--|-->/g,
    to: '',
    allowEmptyPaths: false,
};

function getDateStr() {
    const date = new Date();
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours();

    return year + (monthIndex < 10 ? '0' + monthIndex : monthIndex) + (day < 10 ? '0' + day : day) + '_' + hour;
}

try {
    replace.sync(options);
    console.log('Build version set: ' + buildVersion);
    replace.sync(servicesOptions);
    console.log('app.services pointing to real serve');
    replace.sync(indexOptions);
    console.log('index comments removed');
}
catch (error) {
    console.error('Error occurred:', error);
}
