export const environment = {
  production: false,
  hostUrl: '',
  hostHeaders: {
    'Accept': 'application/vnd.idk.med-v1+json',
    'Accept-Language': 'es-ES',
    'Content-Type': 'application/json',
  },
  languages: [
    'es-ES'
  ],
  defaultLanguage: 'es-ES',
  buildVersion: ''
};
