export const environment = {
  production: false,
  hostUrl: 'http://localhost:8080/',
  hostHeaders: {
    'Accept': 'application/vnd.idk.med-v1+json',
    'Accept-Language': 'es-ES',
    'Content-Type': 'application/json',
  },
  languages: [
    'es-ES'
  ],
  defaultLanguage: 'es-ES',
  buildVersion: ''
};