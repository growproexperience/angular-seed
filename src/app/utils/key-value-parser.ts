export class KeyValueParser {
    static parse = function(objects: Array<any>, keyField, valueField) {
        return objects.map((object) => {
            return {
                key: object[keyField],
                value: object[valueField]
            };
        });
    };
}
