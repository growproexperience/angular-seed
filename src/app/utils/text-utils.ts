export class TextUtils {

    static normalize(text: string): string {
        if (!text) return '';
        else return text.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    }
}
