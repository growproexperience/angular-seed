import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslationsReadyResolve } from './services/resolvers/translation.resolve.service';
import { HomeComponent } from './views/home/home.component';


const routes: Routes = [
  {
    path: '', resolve: { translation: TranslationsReadyResolve },
    children: [
      {
        path: 'home', 
        children: [
          {
            path: '',
            component: HomeComponent
          }
        ]
      }
    ]
  }
];

export const APP_VIEWS = [
  HomeComponent
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule],
  providers: [TranslationsReadyResolve]
})

export class AppRoutingModule { }
