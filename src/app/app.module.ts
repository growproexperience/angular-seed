import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { APP_COMPONENTS } from './components/app.components';
import { APP_DIRECTIVES } from './directives/app.directives';
import { APP_PIPES } from './pipes/app.pipes';
import { AppRoutingModule, APP_VIEWS } from './routing.module';
import { APP_SERVICES } from './services/app.services';
import { AppTranslateModule } from './translate.module';
import { APP_MODALS } from './modals/app.modals';


@NgModule({
  declarations: [
    AppComponent,
    APP_VIEWS,
    APP_COMPONENTS,
    APP_MODALS,
    APP_PIPES,
    APP_DIRECTIVES
  ],
  imports: [
    // Angular core Modules
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // Config modules
    AppRoutingModule,
    AppTranslateModule,
    // Custom Modules
  ],
  entryComponents: [
    APP_COMPONENTS,
    APP_MODALS,
  ],
  providers: [
    APP_SERVICES
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ],
})
export class AppModule {
}
