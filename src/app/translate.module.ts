import {NgModule} from '@angular/core';

import {TranslateModule} from '@ngx-translate/core';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';

declare var require: any;

@NgModule({
  imports: [TranslateModule.forRoot()],
  exports: [TranslateModule],
  providers: [
    TranslateService
  ]
})
export class AppTranslateModule {

  constructor(private translate: TranslateService) {
    this.translate.addLangs(environment.languages);
    this.translate.setDefaultLang(environment.defaultLanguage);

    for (const lang of environment.languages) {
      this.translate.setTranslation(lang, require('../assets/i18n/' + lang + '.json'));
    }
  }
}
