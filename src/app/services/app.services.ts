// Data Services
import { LocationStrategy, PathLocationStrategy, APP_BASE_HREF } from '@angular/common';
import { Injector } from '@angular/core';
// Other services
import { AuthDataService } from './remote-data/auth.service';
import { AuthDataServiceMock } from './remote-data/mock/auth.service';

export const APP_SERVICES = [
  // Data services
  { provide: AuthDataService, useClass: AuthDataServiceMock, deps: [Injector] },
 
  // Other services
  { provide: APP_BASE_HREF, useValue: './' },
  { provide: LocationStrategy, useClass: PathLocationStrategy }
];
