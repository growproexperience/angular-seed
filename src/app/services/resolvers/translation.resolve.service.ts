import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Injectable()
export class TranslationsReadyResolve implements Resolve<boolean> {
  constructor(private translateService: TranslateService) { }

  resolve(): Observable<any> {
    return this.translateService.get('init_translation');
  }
}
