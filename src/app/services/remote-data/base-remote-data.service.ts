import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AuthDataService } from './auth.service';


export class BaseRemoteDataService {
    private _headers: HttpHeaders;
    private authService: AuthDataService;

    private httpClient: HttpClient;
    constructor(public injector: Injector) {
        this.httpClient = injector.get(HttpClient);
        this.authService = injector.get(AuthDataService);

        const headers = environment.hostHeaders;

        this._headers = new HttpHeaders().set('Accept-Language', headers['Accept-Language']);
    }

    get(path: string, data?: any): Promise<any> {
        const requestData = {
            params: {
                data: data,
                path: path,
                method: 'GET'
            }
        };
        return this._request(requestData);
    }

    post(path: string, data?: any): Promise<any> {
        const requestData = {
            params: {
                data: data,
                path: path,
                method: 'POST'
            }
        };
        return this._request(requestData);
    }

    put(path: string, data?: any): Promise<any> {
        const requestData = {
            params: {
                data: data,
                path: path,
                method: 'PUT'
            }
        };
        return this._request(requestData);
    }

    delete(path: string, data?: any): Promise<any> {
        const requestData = {
            params: {
                data: data,
                path: path,
                method: 'DELETE'
            }
        };
        return this._request(requestData);
    }

    private _request(requestData): Promise<any> {
        const userRequestData = this._getUserRequestData();
        const body = Object.assign(requestData.params.data || {}, userRequestData);
        let observable: Observable<any>;
        return new Promise(resolve => {
            switch (requestData.params.method) {
                case 'PATCH':
                case 'PUT':
                case 'POST': {
                    let httpRequest: HttpRequest<any> = new HttpRequest(requestData.params.method, `${environment.hostUrl}/${requestData.params.path}`, body, {
                        headers: this._headers
                    });
                    observable = this.httpClient.request(httpRequest);
                    break;
                }
                case 'DELETE':
                case 'HEAD':
                case 'OPTIONS':
                case 'GET': {
                    let httpRequest = new HttpRequest(requestData.params.method, `${environment.hostUrl}/${requestData.params.path}`, {
                        headers: this._headers
                    });
                    observable = this.httpClient.request(httpRequest);
                    break;
                }
            }
            observable.subscribe((value) => {
                let result: any = value;
                if (!!result && result instanceof HttpResponse) {

                    if (result.body) {
                        if (
                            result.body.operationResult &&
                            !result.body.descriptionError
                        ) {
                            resolve(result.body);
                        } else {
                            throw {
                                statusCode: result.status,
                                message: result.body.descriptionError
                            };
                        }
                    } else {
                        throw {
                            statusCode: result.status,
                            message: 'Unexpected error'
                        };
                    }
                }
            }, error => {
                console.log(error);
            });
        });
    }

    private _getUserRequestData() {
        const loggedUserData = this.authService.userData.value;
        const userRequestData: any = {};
        if (!!loggedUserData) {
            // TODO: manage user session
            // userRequestData.user = loggedUserData.id,
            // userRequestData.token = loggedUserData.token,
            // userRequestData.language = environment.defaultLanguage
        }
        return userRequestData;
    }
}
