import { Observable ,  BehaviorSubject } from 'rxjs';
import { Injectable } from "@angular/core";
import { UserModel } from "../../models/user.model";

@Injectable()
export class AuthDataService {
  public userData: BehaviorSubject<UserModel> = null;

  public writeData(data: any) {
    if (!!data) {
      let user = new UserModel(data);
      this.userData = new BehaviorSubject<UserModel>(user);
    }
  }

  public userObservable(): Observable<UserModel> {
    return this.userData.asObservable();
  }

  public getData(): UserModel {
    return this.userData.getValue();
  }
}
