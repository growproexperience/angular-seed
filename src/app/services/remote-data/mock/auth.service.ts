import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { AUTH_USER } from '../../../mockdata/auth/auth-user';
import { UserModel } from "../../../models/user.model";

@Injectable()
export class AuthDataServiceMock {
  public userData: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(new UserModel(AUTH_USER));

  constructor() {
  }

  public writeData(data: any) {
    data = AUTH_USER;
    let user = new UserModel(data);
    this.userData = new BehaviorSubject<UserModel>(user);
  }

  public userObservable(): Observable<UserModel> {
    return this.userData.asObservable();
  }

  public getData(): UserModel {
    return this.userData.getValue();
  }
}