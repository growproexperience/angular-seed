export class UserModel {
  constructor(data: any) {
    this.fromJSON(data);
  }

  private _id: string | number;
  get id(): string | number {
    return this._id;
  }

  private _firstName: string;
  get firstName(): string {
    return this._firstName;
  }

  private _middleName: string;
  get middleName(): string {
    return this._middleName;
  }

  private _lastName: string;
  get lastName(): string {
    return this._lastName;
  }

  get fullName(): string {
    return `${this._firstName} ${this._middleName} ${this._lastName}`;
  }

  fromJSON(data: any) {
    this._id = data.id;
    this._firstName = data.firstName;
    this._middleName = data.middleName;
    this._lastName = data.lastName;
  }

  toJSON() {
    return {
      id: this._id,
      firstName: this._firstName,
      middleName: this._middleName,
      lastName: this._lastName,
    };
  }
}
