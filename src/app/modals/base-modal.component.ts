import { HostBinding } from "@angular/core";

export class BaseModalComponent {

    @HostBinding('class') classes = 'modal';
    public visibleAnimate = false;

    // TODO: publicData

    public show(): void {
        this.classes = 'modal shown';
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        // Todo: when we close the modal, pass the params to other page, with this.publicData
        this.visibleAnimate = false;
        setTimeout(() => this.classes = 'modal', 300);
    }

    /*public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }*/
}