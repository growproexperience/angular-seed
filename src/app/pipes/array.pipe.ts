import {Pipe, PipeTransform} from '@angular/core';

@Pipe({ name: 'filterBy' })
export class ArrayFilterByPipe implements PipeTransform {
  transform(arr: Array<any>, value: string = '', field: string = 'nombre'): Array<any> {
    return (arr || []).filter(item => {
      return item[field].toLowerCase().indexOf(value.toLowerCase()) >= 0
    });
  }
}
