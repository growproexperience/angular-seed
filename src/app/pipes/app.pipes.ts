import { ArrayFilterByPipe } from './array.pipe';
import { SafeHtmlPipe } from './html-sanitizer.pipe';

/**
 * Contains all the declarations of the different pipes used in our app
 */
export const APP_PIPES = [
  ArrayFilterByPipe,
  SafeHtmlPipe
];
